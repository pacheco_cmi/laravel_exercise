<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Players extends Model
{
	protected $table = 'players';

	protected $primary_key = 'id';

   	protected $fillable = [
   		'compiled_name', 'first_name',
        'second_name', 'web_name',
        'photo', 'code'
   	];

   	protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    // insert data inserted and prepare statment for use in player_info
    // return array for use in other models with the removal of this model's fillables
    // note: include last get id in array returned
    public function insertNew ($data) {
    	$arrPar = [
    		'created_at' => date("Y-m-d H:i:s"),
    		'updated_at' => date("Y-m-d H:i:s"),
    	];

    	foreach ( $this->fillable as $key ) {
    		if( array_key_exists($key, $data) ){
    			$arrPar[$key] = $data->$key;
    			unset($data->$key);
    		}
    	}
    	// combine for compiled name
    	$arrPar['compiled_name'] = implode(", ", [
    		$arrPar['second_name'], $arrPar['first_name']
    	]);

    	$idcheck = DB::table($this->table)->where('compiled_name',$arrPar['compiled_name'])->first();
    	if( !$idcheck ) 
			$data->player_id = DB::table($this->table)->insertGetID($arrPar);
    	else $data->player_id = $idcheck->id;
    	
    	return $data;
    }
}
