<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Player_info extends Model
{
	protected $table = 'player_info';

	protected $primary_key = 'id';

    protected $fillable = [ 'player_id', 'stat', 'value' ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function updateAllPlayerInfo($data) {
    	// get player Id from generic array and unset it as it is separated
    	$player_id = $data->player_id;
    	unset($data->player_id);

    	// remove traces of previous Info since this is a batch replace
    	// note: update per stat should be done separately
    	DB::table($this->table)->where('player_id', $player_id)->delete();

    	$arrSave = [];
    	foreach ( $data as $k=>$v )
    		$arrSave[] = [
    			'player_id' => $player_id, 'stat'=>$k, 
    			'value'=>isset($v)?$v:0,
    			'created_at' => date("Y-m-d H:i:s"),
				'updated_at' => date("Y-m-d H:i:s"),
    		];
    	    	
    	// batch insert for one time sql session
		return DB::table($this->table)->insert($arrSave);
    }

}
