<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Players;
use App\Player_info;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index($page=1) {
    	// using curl instead of guzzle as https seems to be working weirdly with guzzle
    	$ch = curl_init('https://fantasy.premierleague.com/api/bootstrap-static/');
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	$resp = curl_exec($ch);
    	curl_close($ch);

    	// process json response into a readable object
    	$data = json_decode($resp);
    	// please change into xml here if need be.

    	// per page chunk so server load is eased a little
    	$datachunk = array_chunk($data->elements, 100); 

    	if($page > count($datachunk))
			die(json_encode([
	    		'rows'=>0, 'page'=>$page,
	    		'message'=>'Reached Maximum Page!',
	    	]));

    	$players = new Players();
    	$p_info = new Player_info();
    	foreach ($datachunk[$page-1] as $element) {
    		$croppedArr = $players->insertNew($element);
    		$p_info->updateAllPlayerInfo($croppedArr);
    	}

    	print(
    		json_encode([
	    		'rows'=>count($datachunk[$page-1]), 
	    		'message'=>'Successfully inserted rows!',
	    		'page'=>$page,
	    		'maxpages'=>count($datachunk),
	    	])
	    );
    }

    public function list_players() {
    	$players = Players::all();
    	$list = [];
    	foreach( $players as $player ) {
    		$rowlist = [];
    		foreach (['id', 'compiled_name'] as $each)
    			$rowlist[$each] = $player->$each;
	    	$list[] = $rowlist;
    	}
    	echo (json_encode($list));
    }

    public function view_player($id=false) {
    	if( !$id ) die(json_encode(['err'=>'404', 'message'=>'Player Inexistent!']));

    	$data = [];
    	$player = Players::find($id);

    	if( !$player ) die(json_encode(['err'=>'404', 'message'=>'Player Inexistent!']));

    	$maincols = ['id', 'first_name', 'second_name', 'web_name', 'photo', 'code'];
    	foreach ( $maincols as $col)
    		$data[$col] = $player->$col;

    	$playerinfo = Player_info::where('player_id', $id)->get();
    	foreach($playerinfo as $row)
    		$data[$row->stat] = $row->value;
    	
    	echo (json_encode($data));
    }
}