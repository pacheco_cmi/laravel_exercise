<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/retrieve', 'Controller@index');
Route::get('/retrieve/{page}', 'Controller@index');
Route::get('/list', 'Controller@list_players');
Route::get('/view/{id}', 'Controller@view_player');